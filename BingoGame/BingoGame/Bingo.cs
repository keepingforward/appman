﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BingoGame
{
    class Bingo
    {
        static private int _dim = 5;
        static private int[][] _BingoTable;

        static Bingo()
        {
            _BingoTable = new int[_dim][];

            for (int i = 0; i < _dim; i++)
            {
                int[] col = new int[_dim];

                for (int j = 0; j < _dim; j++)
                {
                    col[j] = ((i * _dim) + 1) + j;
                }

                _BingoTable[i] = col;
            }
        }

        private static int[][] _Mark(int[] input)
        {
            int[][] tmpTable = new int[_dim][];

            for (int i = 0; i < _dim; i++)
            {
                int[] col = new int[_dim];

                for (int j = 0; j < _dim; j++)
                {
                    col[j] = Array.IndexOf<int>(input, _BingoTable[i][j]) > -1 ? 1 : 0; // ((i * _col) + 1) + j;
                }

                tmpTable[i] = col;
            }

            return tmpTable;
        }

        private static void PrintTable(int[][] array)
        {
            Console.WriteLine("==========================");
            for (int i = 0; i < _dim; i++)
            {
                Console.WriteLine(string.Join(", ", array[i]));
            }
            Console.WriteLine("==========================");
        }

        public static void PrintTableBase()
        {
            PrintTable(_BingoTable);
        }

        public static bool IsBingo(int[] input)
        {
            Console.WriteLine($"input: {string.Join(", ", input)}");

            var mark = _Mark(input);
            // PrintTable(mark);

            int clossSlashSum = 0; // [/]
            int clossBackSlashSum = 0; // [\]

            // row checked
            for (int i = 0; i < _dim; i++)
            {
                int rowSum = 0;
                int colSum = 0;

                clossBackSlashSum += mark[i][i];
                clossSlashSum += mark[i][_dim - i - 1];

                for (int j = 0; j < _dim; j++)
                {
                    rowSum += mark[i][j];
                    colSum += mark[j][i];
                }

                if (rowSum >= _dim || colSum >= _dim)
                    return true;
            }

            if (clossBackSlashSum >= _dim || clossSlashSum >= _dim)
                return true;

            // // colume
            // for (int i = 0; i < _dim; i++)
            // {
            //     int colSum = 0;

            //     for (int j = 0; j < _dim; j++)
            //     {
            //         colSum += mark[j][i];
            //     }

            //     if (colSum >= _dim)
            //         return true;
            // }

            return false;
        }
    }
}
