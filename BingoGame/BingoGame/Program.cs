﻿using System;

namespace BingoGame
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("===============================================================");
            Console.WriteLine("Bingo Table");
            Bingo.PrintTableBase();

            Console.WriteLine();
            Console.WriteLine("===============================================================");
            Console.WriteLine("From appman");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine($"{(Bingo.IsBingo(new int[] { 3, 4, 8, 13, 18, 19, 23 }) ? "Bingo" : "NotBingo")}\r\n");
            Console.WriteLine($"{(Bingo.IsBingo(new int[] { 1, 13, 19, 25, 23, 2 }) ? "Bingo" : "NotBingo")}\r\n");
            Console.WriteLine($"{(Bingo.IsBingo(new int[] { 2, 1, 12, 15, 6, 18, 16, 4, 3, 21, 11 }) ? "Bingo" : "NotBingo")}\r\n");

            Console.WriteLine();
            Console.WriteLine("===============================================================");
            Console.WriteLine("other test must 'Bingo'");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine($"{(Bingo.IsBingo(new int[] { 1, 2, 3, 4, 5 }) ? "Bingo" : "NotBingo")}\r\n");
            Console.WriteLine($"{(Bingo.IsBingo(new int[] { 5, 10, 15, 20, 25 }) ? "Bingo" : "NotBingo")}\r\n");
            Console.WriteLine($"{(Bingo.IsBingo(new int[] { 1, 7, 13, 19, 25 }) ? "Bingo" : "NotBingo")}\r\n");
            Console.WriteLine($"{(Bingo.IsBingo(new int[] { 5, 9, 13, 17, 21 }) ? "Bingo" : "NotBingo")}\r\n");

            Console.WriteLine();
            Console.WriteLine("===============================================================");
            Console.WriteLine("other test must 'No Bingo'");
            Console.WriteLine("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
            Console.WriteLine($"{(Bingo.IsBingo(new int[] { 1, 2, 4, 5 }) ? "Bingo" : "NotBingo")}\r\n");
            Console.WriteLine($"{(Bingo.IsBingo(new int[] { 5, 10, 20, 25 }) ? "Bingo" : "NotBingo")}\r\n");

        }
    }
}
