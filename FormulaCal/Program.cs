﻿using System;

namespace FormulaCal
{
    class Program
    {
        static void Main(string[] args)
        {
            // mind
            PrintAspect("", 0);
            PrintAspect("1", 1);
            PrintAspect("1+2", 3);
            PrintAspect("1+2-1", 2);
            PrintAspect("1+2*1", 3);
            PrintAspect("4-2", 2);
            PrintAspect("5*6", 30);
            PrintAspect("5*6-4", 10);
            PrintAspect("12/4", 3);
            PrintAspect("12/4+2", 2);
            PrintAspect("10-5+3/2*2", 8);

            PrintAspect("(1)", 1);
            PrintAspect("((9))", 9);
            PrintAspect("(1+2)", 3);
            PrintAspect("(1+2-1)", 2);
            PrintAspect("(1+2*1)", 3);
            PrintAspect("(4-2)", 2);
            PrintAspect("(5*6)", 30);
            PrintAspect("(5*6-4)", 10);
            PrintAspect("(12/4)", 3);
            PrintAspect("(12/4+2)", 2);
            PrintAspect("(10-5+3/2*2)", 8);

            var lastForegroundColor = Console.ForegroundColor;
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("============================================================================");
            Console.ForegroundColor = lastForegroundColor;

            // from appman
            PrintAspect("(22*2)+50", 94);
            PrintAspect("((2*3+12)/2)", 15);
            PrintAspect("(10-5+3/2*2)", 8);

            // // Must Error
            // PrintAspect("(10-5+3/2*2a", 8);
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("============================================================================");
            Console.ForegroundColor = lastForegroundColor;
        }

        static void PrintAspect(string statement, double aspect)
        {
            // Console.WriteLine("============================================================================");
            // Console.WriteLine($"  {statement}");
            var result = Calculator.Cal(statement);

            // Console.WriteLine($"  {statement}");
            // Console.WriteLine($"(({statement} = {result}) == {aspect}) is {((long)result) == ((long)aspect)}\r\n");

            var lastForegroundColor = Console.ForegroundColor;
            bool isMatched = ((long)result) == ((long)aspect);
            Console.Write("(( ");

            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.Write(statement);
            Console.ForegroundColor = lastForegroundColor;
            Console.Write(" == ");
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.Write(aspect);
            Console.ForegroundColor = lastForegroundColor;
            Console.Write(" ) == ");
            Console.ForegroundColor = ConsoleColor.DarkCyan;
            Console.Write(result);
            Console.ForegroundColor = lastForegroundColor;
            Console.Write(" ) is ");
            Console.ForegroundColor = isMatched ? ConsoleColor.Green : ConsoleColor.Red;
            Console.WriteLine(isMatched);

            Console.ForegroundColor = lastForegroundColor;
        }
    }
}
