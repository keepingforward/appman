﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FormulaCal
{
    class Calculator
    {
        // เรียงความสำคัญจากน้อยไปมาก
        // +, - มีค่ามากสุด
        private static readonly char[] _Operators = new char[] { '/', '*', '-', '+' };
        private static readonly char[] _Brackets = new char[] { '(', ')' };
        private static bool IsOperators(char c)
        {
            return (Array.IndexOf<char>(_Operators, c) > -1);
        }

        private static bool IsValidateChar(char c)
        {
            return char.IsDigit(c) || IsOperators(c) || IsBrackets(c);
        }

        private static bool IsBrackets(char c)
        {
            return IsOpenBracket(c) || IsCloseBracket(c);
        }

        private static bool IsOpenBracket(char c)
        {
            return c == '(';
        }

        private static bool IsCloseBracket(char c)
        {

            return c == ')';
        }

        private static string _ClearSpace(string input)
        {
            // Cr. BlueChippy
            // https://stackoverflow.com/questions/6219454/efficient-way-to-remove-all-whitespace-from-string
            string newText = input
                .ToCharArray()
                .Where(c => !Char.IsWhiteSpace(c))
                .Select(c => c.ToString())
                .Aggregate((a, b) => a + b);

            // อนุญาตเฉพาะ ตัวเลขและ cal operators
            foreach (var c in newText)
            {
                if (!IsValidateChar(c))
                {
                    throw new Exception($"Not support '{c}' operator, in '{newText}'");
                }
            }

            return newText;
        }

        private static Stack<StateElement> _ProcessStack(string clearSpaceText)
        {
            var tmp = clearSpaceText.ToCharArray();
            List<StateElement> elements = new List<StateElement>();
            bool lastIndexIsdigit = false;

            foreach (var c in tmp)
            {
                if (char.IsDigit(c))
                {
                    StateElement lastEl = null;
                    double number = Convert.ToDouble(c.ToString());

                    if (lastIndexIsdigit)
                    {
                        lastEl = elements.Last();
                        lastEl.NumValue = (lastEl.NumValue * 10) + number;
                    }
                    else
                    {
                        elements.Add(new StateElement()
                        {
                            IsNumber = true,
                            NumValue = number
                        });
                    }

                    lastIndexIsdigit = true;
                    continue;
                }
                else if (IsOpenBracket(c))
                {
                    elements.Add(new StateElement()
                    {
                        IsOpenBracket = true,
                        Operator = c
                    });
                }
                else if (IsCloseBracket(c))
                {
                    elements.Add(new StateElement()
                    {
                        IsCloseBracket = true,
                        Operator = c
                    });
                }
                else if (IsOperators(c))
                {
                    elements.Add(new StateElement()
                    {
                        IsOperator = true,
                        Operator = c
                    });
                }
                else
                {
                    throw new Exception("Unknow ...");
                }

                lastIndexIsdigit = false;
            }

            var stack = new Stack<StateElement>(new Stack<StateElement>(elements));
            // while (true)
            // {
            //     if (stack.Count == 0) break;

            //     var el_from_stack = stack.Pop();
            //     Console.WriteLine($"### : {el_from_stack}");
            // }

            return stack;
        }

        public class StateElement
        {
            public char Operator { get; set; }
            public double NumValue { get; set; }
            public bool IsCloseBracket { get; set; }
            public bool IsOpenBracket { get; set; }
            public bool IsNumber { get; set; }
            public bool IsOperator { get; set; }

            public override string ToString()
            {
                return IsNumber ? NumValue.ToString() : Operator.ToString();
            }
        }

        internal static double Cal(string state)
        {
            if (state == null || state.Trim().Length == 0) return 0;

            var clearSpaceText = _ClearSpace(state);
            var stack = _ProcessStack(clearSpaceText);
            var value = _Process(stack);

            return value;
        }

        private static double _Process(Stack<StateElement> stack)
        {
            if (stack.Count == 0) return 0;

            return _CommonState(stack);
        }

        private static double _CommonState(Stack<StateElement> stack)
        {
            while (stack.Count != 0)
            {
                var firstEL = stack.Pop();
                if (stack.Count == 0)
                {
                    if (!firstEL.IsNumber) throw new Exception($"last char, must be number or close bracket: {firstEL.NumValue}");
                    return firstEL.NumValue;
                }
                else
                {
                    var secoundEL = stack.Pop();
                    if (firstEL.IsOpenBracket)
                    {
                        stack.Push(secoundEL);

                        var nestedInBracketValue = _CommonState(stack);

                        var mustBeCloseBracket = stack.Pop();
                        if (!mustBeCloseBracket.IsCloseBracket) throw new Exception($"Must be close-bracket: {mustBeCloseBracket.ToString()}");

                        stack.Push(new StateElement()
                        {
                            IsNumber = true,
                            NumValue = nestedInBracketValue
                        });
                    }
                    if (firstEL.IsNumber && secoundEL.IsCloseBracket)
                    {
                        // Console.WriteLine($"firstEL: {firstEL.ToString()}, secoundEL: {secoundEL.ToString()}");
                        stack.Push(secoundEL);

                        return firstEL.NumValue;
                    }
                    else if (secoundEL.IsOperator)
                    {
                        var thirdEL = stack.Pop();
                        var a = firstEL.NumValue;
                        var b = secoundEL.Operator;

                        if (thirdEL.IsNumber)
                        {
                            if (stack.Count == 0)
                            {
                                stack.Push(new StateElement()
                                {
                                    IsNumber = true,
                                    NumValue = _SimpleCalculation(a, b, thirdEL.NumValue)
                                });
                            }
                            else if (stack.Count > 0)
                            {
                                var fourthEL = stack.Pop();

                                if (fourthEL.IsCloseBracket)
                                {
                                    stack.Push(fourthEL);
                                    stack.Push(new StateElement()
                                    {
                                        IsNumber = true,
                                        NumValue = _SimpleCalculation(a, b, thirdEL.NumValue)
                                    });
                                }
                                else if (fourthEL.IsOperator)
                                {
                                    if (IsFirstOperatorWeightIsBetter(b, fourthEL.Operator))
                                    {
                                        stack.Push(fourthEL);
                                        stack.Push(new StateElement()
                                        {
                                            IsNumber = true,
                                            NumValue = _SimpleCalculation(a, b, thirdEL.NumValue)
                                        });
                                    }
                                    else
                                    {
                                        stack.Push(fourthEL);
                                        stack.Push(thirdEL);
                                        var nestedValue = _CommonState(stack); //_SimpleCalculation(a, b, _CommonState(stack));

                                        stack.Push(new StateElement()
                                        {
                                            IsNumber = true,
                                            NumValue = _SimpleCalculation(a, b, nestedValue)
                                        });
                                    }
                                }
                            }
                        }
                    }
                }
            }

            return 0;
        }

        private static bool IsFirstOperatorWeightIsBetter(char prefixOperator, char suffixOperator)
        {
            return Array.IndexOf<char>(_Operators, prefixOperator) >= Array.IndexOf<char>(_Operators, suffixOperator);
        }

        private static double _SimpleCalculation(double prefix, char op, double suffix)
        {
            switch (op)
            {
                case '+':
                    return prefix + suffix;
                case '-':
                    return prefix - suffix;
                case '*':
                    return prefix * suffix;
                case '/':
                    return prefix / suffix;
                default:
                    throw new Exception($"Not support '{op}' operator.");
            }
        }
    }
}
